

graph= {'A': ['B', 'F',],
         'B': ['A', 'C', 'D', 'E'],
         'C': ['B', 'E', 'H'],
         'D': ['B', 'E', 'F'],
         'E': ['B', 'C', 'D', 'F', 'G', 'H'],
         'F': ['A', 'D', 'E', 'G'],
         'G': ['E', 'F'],
         'H': ['C', 'E']}
    


def bfs(G, s, objetivo):
    found = False
    fringe=[s]
    level={s:0} 
    i=1
    while fringe and not found:
        childs = []
        for U in fringe:
            for v in G[U]:
                if v in level and not found:
                    level[v]=i
                    childs.append(v)
                if v == objetivo:
                    found = True
        fringe 
    i=i+1            


result = bfs(graph, 'A', 'G')

print "Ruta: ", result, "Distancia: ", len(result)